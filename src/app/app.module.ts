import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { TopBarComponent } from './components/header/top-bar/top-bar.component';
import { TopMenuComponent } from './components/header/top-menu/top-menu.component';
import { HeroComponent } from './components/hero/hero.component';
import { CoverComponent } from './components/cover/cover.component';
import { DeliveryAddressComponent } from './components/hero/delivery-address/delivery-address.component';
import { CarouselComponent } from './components/hero/carousel/carousel.component';
import { AboutComponent } from './components/about/about.component';
import { HowItWorksComponent } from './components/about/how-it-works/how-it-works.component';
import { GiftComponent } from './components/gift/gift.component';
import { GiveGiftComponent } from './components/gift/give-gift/give-gift.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TopBarComponent,
    TopMenuComponent,
    HeroComponent,
    CoverComponent,
    DeliveryAddressComponent,
    CarouselComponent,
    AboutComponent,
    HowItWorksComponent,
    GiftComponent,
    GiveGiftComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
